<?php
	session_start();
	require_once("config/database.php");

	if (!isset($_POST["usermail"]) || !isset($_POST["userpassword"]))
	{
		echo "ERREUR DANS LE FORMULAIRE RECU";
		die ;
	}
	$currentUserSQL = "SELECT * FROM camagru_users WHERE userMail LIKE :userMail AND userPassword LIKE :userPassword";
	$req = $db->prepare($currentUserSQL);
	$req->execute(
		array(
			":userMail" => $_POST["usermail"],
			":userPassword" => $_POST["userpassword"]
		)
	);
	$userInfo = $req->fetchAll();
	if (count($userInfo) > 0)
		echo "exists";
	else
		echo "not exists";
	$_SESSION["userInfo"] = $userInfo;
	if (count($_SESSION["userInfo"]))
		echo "SESSION EXISTS";
	else
		echo "SESSION NOT EXISTS";
?>
