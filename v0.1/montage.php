<?php
require 'global.php';
	if (!isset($_SESSION['username']))
	{
		$_SESSION['message'] = "you must be authentiicated";
		header("Location: index.php");
	}

  $stickerList = Array(
    'img/navire.png',
    'img/apple.png',
    'img/lidl.png',
    'img/tete.png',
    'img/orangina.png',
    'img/wow.png'
  );

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
  <meta charset="utf-8">
  <title>Camagru</title>
  <link rel="stylesheet" href="css/modal.css">
  <link rel="stylesheet" type="text/css" href="css/montage.css">
</head>

<body>
  <div class="header">
      <a href="logout.php">
      <img src="img/logout.png" class="logoOut">
      </a>
  </div>
  
  <div class="main"> 
    <div class="menuLeft">
      <?php
        foreach ($stickerList as $sticker) {
          echo '
            <div onclick="stickerfunc(this)">
              <img src="' . $sticker .'">
            </div>
          ';
        }
      ?>
    </div>
    
    <div class="menuRight">
      <?php
        $owner = $_SESSION['username'];
        try
        {
          $dbh = new PDO("mysql:host=localhost;dbname=camagruDB", "root", "root");
          array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
        }
        catch (exception $e)
        {
          die('Erreur : '.$e->getMessage());
        }
        $sql = $dbh->prepare("SELECT path FROM photos WHERE owner = ? ORDER BY id DESC");
        $sql->execute(array($owner));
        while ($content = $sql->fetch())
        {
          echo "<div><img src='".$content[path]."'></div>";
        }
      ?>
    </div>
    
    <div class="content">
      <video id="video"></video>
      <div class="rowCenter">
        <button  class="snap" id="startbutton" >Take picture</button>
        <button class="upload">Upload picture</button>
      </div>
      <canvas class="rowCenter" id="canvas"></canvas>
    </div>
      
  </div>

<div class='footer'></div>
<script type="text/javascript" src="webcam.js"></script>
</body>
</html>
