<?php
  require 'global.php';
	if (!isset($_SESSION['username']))
	{
		$_SESSION['message'] = "you must be authentiicated";
		header("Location: index.php");
	}
  try
  {
    $dbh = new PDO("mysql:host=localhost;dbname=camagruDB", "root", "root");
    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
  }
  catch (exception $e)
  {
    die('Erreur : '.$e->getMessage());
  }

  $sql = $dbh->prepare("SELECT path FROM photos ORDER BY id DESC");
  $sql->execute();
  $pics = $sql->fetchAll();

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
  <meta charset="utf-8">
  <title>Camagru</title>
  <link rel="stylesheet" href="css/modal.css">
  <link rel="stylesheet" type="text/css" href="css/montage.css">
</head>

<body>
  <div class="header">
      <a href="logout.php">
      <img src="img/logout.png" class="logoOut">
      </a>
      <a href="montage.php">
        <img src="img/cam.jpg" class="galerie">
      </a>
  </div>
  
  <div class="main"> 
    <div class="contentGal">
      <?php
        foreach ($pics as $pic) {      
          $sql = $dbh->prepare("SELECT likes FROM photos WHERE path = ?");
          $sql->execute(array($pic));
          $likes = $sql->fetch();
          if (!$likes)
            $likes = 0;
          echo '
            <div>
              
              <img class="pic" data-pic-id="'.$pic["id"].'" src="'.$pic["path"].'">
              <div display:inline>
                <p id="nblikes">'.$likes.' likes</p>
                <tr>
                    <td><img src="img/like.jpg" class="logoOut"></td>
                <tr>
              </div>
              <form method="post" action="galerie.php">
                <table>  
                  <tr><td><textarea name="comments" id="com"></textarea></td><tr>
                  <tr><td><input type="submit" name="submit" value="Post Comment"></td><tr>
                </table>
               
              </form>
            </div>
          ';
        }
      ?>
    </div>


<div class='footer'></div>
<!-- <script type="text/javascript" src="webcam.js"></script> -->
</body>
</html>
