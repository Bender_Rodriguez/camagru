<?php
require 'global.php';
	if (!isset($_SESSION['username']))
	{
		$_SESSION['message'] = "you must be authentiicated";
		header("Location: index.php");
	}

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
  <meta charset="utf-8">
  <title>Camagru</title>
  <link rel="stylesheet" href="css/modal.css">
  <link rel="stylesheet" type="text/css" href="css/montage.css">

</head>

<body>
  <div class='header'>
      <a href="logout.php" >
      <img src="img/logout.png" class="logoOut" width=40px; height=40px;>
      </a>
  </div>
  
  <div class='main'> 
  
    <div class="menuLeft">
      <label>
        <input id="navire" type="stick" name="filtre" onclick="stickerfunc(this)">
        <div><img src="img/navire.png" height=75px;></div>
      </label>
      <label>
        <input id="apple" type="stick" name="filtre" onclick="stickerfunc(this)">
        <div><img src="img/apple.png" height=75px;></div>
      </label>
      <label>
        <input id="lidl" type="stick" name="filtre" onclick="stickerfunc(this)">
        <div><img src="img/lidl.png" height=75px;></div>
      </label>
      <label>
        <input id="tete" type="stick" name="filtre" onclick="stickerfunc(this)">
        <div><img src="img/tete.png" height=75px;></div>
      </label>
      <label>
        <input id="oran" type="stick" name="filtre" onclick="stickerfunc(this)">
        <div><img src="img/orangina.png" height=75px;></div>
      </label>
      <label>
        <input id="wow" type="stick" name="filtre" onclick="stickerfunc(this)">
        <div><img src="img/wow.png" height=75px;></div>
      </label>

<!--       <img src="img/navire.png" width=75px; height=75px;>
      <img src="img/apple.png" width=75px; height=75px;>
      <img src="img/lidl.png" width=75px; height=75px;>
      <img src="img/tete.png" width=75px; height=75px;>
      <img src="img/orangina.png" width=75px; height=75px;>
      <img src="img/wow.png" width=75px; height=75px;> -->
   
    </div>
    
    <div class="menuRight">
      <?php
        $owner = $_SESSION['username'];
        try
        {
          $dbh = new PDO("mysql:host=localhost;dbname=camagruDB", "root", "root");
          array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
        }
        catch (exception $e)
        {
          die('Erreur : '.$e->getMessage());
        }
        $sql = $dbh->prepare("SELECT path FROM photos WHERE owner = ? ORDER BY id DESC");
        $sql->execute(array($owner));
        while ($content = $sql->fetch())
        {
          echo "<label><input id='".$content[path]."'><div><img src='".$content[path]."'></div></label>";
        }
      ?>
    </div>
    
    <div class="menuCentral">
      <video id="video"></video>
      <button  class="snap" id="startbutton" >Take picture</button>
      <canvas id="canvas"></canvas>
    </div>
      <!-- <img src="" id="photo"></div> -->
      <!-- <img id="output"> -->

       <!-- <button class="snap" onclick="window.location.href = '#openModal';" id="startbutton" >Take picture</button> -->
      
      <!-- <div id="openModal" class="modalDialog">
        <div class="cadre">
          <canvas id="canvas"></canvas>
          <a href="montage.php" title="Close" class="close">X</a>
        </div>
      </div> -->
      

    <script type="text/javascript" src="webcam.js"></script>



  </div>

<div class='footer'></div>
</body>
</html>
