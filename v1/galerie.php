<?php
  require 'global.php';
	if (!isset($_SESSION['username']))
	{
		$_SESSION['message'] = "you must be authentiicated";
		header("Location: index.php");
	}
  try
  {
    $dbh = new PDO("mysql:host=localhost;dbname=camagruDB", "root", "root");
    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
  }
  catch (exception $e)
  {
    die('Erreur : '.$e->getMessage());
  }

  $sql = $dbh->prepare("SELECT * FROM photos ORDER BY id DESC");
  $sql->execute();
  $pics = $sql->fetchAll();
  // $sql = $dbh->prepare("SELECT * FROM")
  $user = $_SESSION['username'];

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
  <meta charset="utf-8">
  <title>Camagru</title>
  <link rel="stylesheet" href="css/modal.css">
  <link rel="stylesheet" type="text/css" href="css/montage.css">
</head>

<body>
  <div class="header">
  <h4 id="user"><?php echo $user ?></h4>

      <a href="logout.php">
      <img src="img/logout.png" class="logoOut">
      </a>
      <a href="montage.php">
        <img src="img/cam.jpg" class="galerie">
      </a>
  </div>
  
  <div class="main"> 
    <div class="contentGal">
      <?php
        foreach ($pics as $pic) {
          $sql = $dbh->prepare("SELECT * FROM comments WHERE photo_id = ? ORDER BY id DESC");
          $sql->execute(array($pic["path"]));
          $coms = $sql->fetchAll();
          echo '
            <div>
              
              <img class="pic" data-pic-id="'.$pic["id"].'" src="'.$pic["path"].'">
              <div display:inline>
                <p id="nblikes">'.$pic["likes"].' likes</p>
                <tr>
                    <td><img src="img/like.jpg" class="logoOut" id="imgsrc" onclick="liker(this)" name="'.$pic["path"].'"></td>
                <tr>
              ';
              foreach ($coms as $com) {
                echo'
                <div>
                  <table>
                    <tr>
                      <td><p>"'.$com["content"].'"    by '.$com["user_id"].'.</p></td>
                    </tr>
                  </table>
                </div>
                ';
              }
              echo '
              </div>
              <form method="post" action="galerie.php">
                <table>  
                  <tr><td><textarea name="comments" id="com"></textarea></td><tr>
                  <tr><td><input onclick="commenter(this)" type="submit" name="'.$pic["path"].'" value="Post Comment"></td><tr>
                </table>
               
              </form>
            </div>
          ';
        }
      ?>
    </div>


<div class='footer'></div>
<!-- <script type="text/javascript" src="webcam.js"></script> -->
<script type="text/javascript">
  
  function commenter(content)
  {   
    var login = document.getElementById("user");
    var user = login.innerText || login.textContent;
    var path = content.getAttribute("name");
    var content = document.getElementById("com");
    content = content.value;

    var ajax = new XMLHttpRequest();
    ajax.open("POST", "comments.php", true);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send("user="+user+"&path="+path+"&content="+content);
    location.reload();
  }

  function liker(elem)
  {
    var ajax = new XMLHttpRequest();
    var login = document.getElementById("user");
    var user = login.innerText || login.textContent;
    var path = elem.getAttribute("name");
    ajax.open("POST", "likes.php", true);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send("user="+user+"&path="+path);
    location.reload();
  }

</script>
</body>
</html>
