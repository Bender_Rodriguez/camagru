<?php

require 'global.php';


$path = $_POST[path];
$user = $_POST[user];




try
{
	$bdd = new PDO("mysql:host=localhost;dbname=camagruDB", "root", "root");
  	array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
}
catch (exception $e)
{
  	die('Erreur : ' . $e->getMessage());
}

$sql = $bdd->prepare("SELECT * FROM likes WHERE photo_id = ? AND user_id = ?");
$sql->execute(array($path, $user));
$already = $sql->fetch();
// if (!$sql->execute(array($path, $user)))
// {
if (!$already)
{
	$sql = $bdd->prepare("INSERT INTO likes (photo_id, user_id) VALUES(?, ?)");
	$sql->execute(array($path, $user));

	$sql = $bdd->prepare("UPDATE photos SET likes = likes + 1 WHERE path = ?");
	$sql->execute(array($path));
}
else
{
	$sql = $bdd->prepare("DELETE FROM likes WHERE photo_id = ? AND user_id = ?");
	$sql->execute(array($path, $user));

	$sql = $bdd->prepare("UPDATE photos SET likes = likes - 1 WHERE path = ?");
	$sql->execute(array($path));
}
?>